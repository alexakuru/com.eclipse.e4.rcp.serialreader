package com.eclipse.e4.rcp.serialreader.eventService;

public interface EventConstants {
	
	String BAUD = "BAUD";
	
	String STOP_BIT = "STOP_BIT";
	
	String PARITY = "PARITY";
	
	String DATA_BIT = "DATA_BIT";
}
