package com.eclipse.e4.rcp.serialreader.handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.MPartSashContainer;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.swt.SWT;

public class ConfigHandler {
	private MWindow window;
	private MPartSashContainer partContainer;
	private MPart portConfig;
	private MPart confView;
	
	@Execute
	public void execute(MApplication application, EPartService partService, EModelService modelService) {
		
		window = modelService.createModelElement(MWindow.class);
		window.setHeight(400);
		window.setWidth(400);
		window.setIconURI("platform:/plugin/com.eclipse.e4.rcp.serialreader/icons/icon2.png");
		window.setLabel("Run Configurations");
		
		partContainer = modelService.createModelElement(MPartSashContainer.class);
		partContainer.setHorizontal(true);
		
		portConfig = modelService.createModelElement(MPart.class);
		portConfig.setContributionURI("bundleclass://com.eclipse.e4.rcp.serialreader"
				+ "/com.eclipse.e4.rcp.serialreader.configview.PortConfig");
		
		confView = modelService.createModelElement(MPart.class);
		confView.setContributionURI("bundleclass://com.eclipse.e4.rcp.serialreader"
				+ "/com.eclipse.e4.rcp.serialreader.configview.ConfigView");
		
		partContainer.getChildren().add(confView);
		partContainer.getChildren().add(portConfig);
		window.getChildren().add(partContainer);
		application.getChildren().add(window);
	}
}