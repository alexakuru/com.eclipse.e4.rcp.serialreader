 
package com.eclipse.e4.rcp.serialreader.handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.ui.IWorkbench;

public class ExitHandler {
	@Execute
	public void execute(IWorkbench workbench) {
		workbench.close();		
	}	
}