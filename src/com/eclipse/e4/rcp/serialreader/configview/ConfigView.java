package com.eclipse.e4.rcp.serialreader.configview;

import javax.annotation.PostConstruct;

import org.eclipse.swt.SWT;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

public class ConfigView {
	private TreeItem mainItem;
	private TreeItem item;
	private Tree tree;
	
	@PostConstruct
	public void createControl(Composite parent) {
		tree = new Tree(parent, SWT.SINGLE | SWT.BORDER);
		tree.setSize(100, 100);
		
		mainItem = new TreeItem(tree, 0);
		mainItem.setText("Stream Settings");
		
		item = new TreeItem(mainItem, 0);
		item.setText("Baud");
		
		item = new TreeItem(mainItem, 0);
		item.setText("Databit");
		item = new TreeItem(mainItem, 0);
		item.setText("Stopbit");
		item = new TreeItem(mainItem, 0);
		item.setText("Parity");
	}
}
